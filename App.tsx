import React from "react";
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { StatusBar } from "react-native";
import Navigation from "./src/services/navigation";
import { isAndroid } from "@freakycoder/react-native-helpers";
import AnimatedSplash from "react-native-animated-splash-screen";

import { configStore } from './src/shared/state/store';
import { DEVICE_WIDTH } from "./src/utils/deviceUtils";
import TopBarPopup from "./src/shared/components/top-bar-popup/TopBarPopup";

const { store, persistor } = configStore();
export { store };
export const dispatch = store.dispatch;
export const persistedStore = persistor;

const App = () => {
  const [isLoaded, setIsLoaded] = React.useState(false);

  React.useEffect(() => {
    StatusBar.setBarStyle("dark-content");
    if (isAndroid) {
      StatusBar.setBackgroundColor("rgba(0,0,0,0)");
      StatusBar.setTranslucent(true);
    }
    setTimeout(() => {
      setIsLoaded(true);
    }, 1350);
  }, []);

  return (
    <Provider store={store} >
      <PersistGate loading={null} persistor={persistor}>
        <AnimatedSplash
          logoWidth={DEVICE_WIDTH * 0.9}
          logoHeight={DEVICE_WIDTH}
          logoImage={require("./src/assets/splash/kittenSplash.png")}
          isLoaded={isLoaded}
          backgroundColor={'#757575'}
          imageBackgroundResizeMode="cover"
        >
          <Navigation />
        </AnimatedSplash>
        <TopBarPopup></TopBarPopup>
      </PersistGate>
    </Provider>
  );
};
App.displayName = 'Kittens'
export default App;
