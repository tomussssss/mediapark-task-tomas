/**
 * @format
 */

import 'react-native';


// Note: test renderer must be required after react-native.
import { getRandomName, getRandomCatPictureIndex } from '../src/utils/catUtils';


test('Random name is string', () => {
  expect(typeof getRandomName()).toBe("string");
});

test('Random index is number and less than 17', () => {
  expect(getRandomCatPictureIndex()).toBeLessThan(17);
  expect(typeof getRandomCatPictureIndex()).toBe("number");
});