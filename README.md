# README #

### What is this repository for? ###

* This is a repository for a job interview project at Mediapark.

### How do I get set up? ###

1. Clone this repository
2. Perform either `yarn install` or `npm install` (Yarn prefered)
3. Launch app with `react-native run-ios/run-android` 

*Or just use the APK provided to run the app*

### Work done ###

1. A minimal test written for utils with `jest` in _test_
2. `FastImage` used for image caching to act as an offline mode, as well as a `persist redux store` along with `Async Storege` created for offline data storing as well.
3. Animated used for smooth header and image (iOs) animations.
4. User input provided as well as tactile buttons to change the count of cat images retrieved. 
5. A clean architectural structure used to keep things clean and easily readable.
6. `Redux-saga` used to hande loader and data side-effects. 
7. Modals used along with NetInfo in `TopBarPopUp.ts` to display a message to the user when the connection is gone.
8. `Pressable` used instead of `TouchableOpacity` for a more future-proof and robust gesture handling solution.
9. Redux used to store data in state.
10. Random names generated for the kittens using an external `npm` package for more versitility.
11. `ActivityIndicator` used to display a loader when needed.
12. `FlatList` used instead of ListView (was specified) because of the performance benefits.
13. Signed APK generated and stored in the root directory.
14. `BitBucket` and `GIT` used for version control.

### Other info and problems encountered ###
1. The api was repetitive so I created a workaround to generate a random index for the image not just get the same ones with the same size. Since `FastImage` is so good at caching and as there are only 16 unique images the API provides you don't really see any loading times.
2. The header animation was a little bit tricky in the last minute, so there is a minor anomaly when you drag the contents to the bottom fast.
3. Image bounce zoom in the description screen doesn't work on android, only on IOS.


### Repo owner or admin: ###
* Tomas Šukelovič
> Email: tomas.sukelovic@gmail.com
