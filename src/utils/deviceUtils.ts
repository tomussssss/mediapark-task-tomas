import { Dimensions } from 'react-native';

export const DEVICE_WIDTH = Dimensions.get('window').width //Can be replaced with useWindowDimensions hook if moved to a react component
export const DEVICE_HEIGTH = Dimensions.get('window').height //Can be replaced with useWindowDimensions hook if moved to a react component

export const getCloser = (value, checkOne, checkTwo) =>
    Math.abs(value - checkOne) < Math.abs(value - checkTwo) ? checkOne : checkTwo;