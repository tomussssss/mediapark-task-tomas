import { uniqueNamesGenerator, Config, names } from 'unique-names-generator';
const config: Config = {
    dictionaries: [names]
}


export const getRandomName = (): string => {
    return uniqueNamesGenerator(config);
}

export const getRandomCatPictureIndex = (): number => {
    return Math.floor(Math.random() * (15) + 1);
}