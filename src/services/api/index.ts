import axios from 'axios';
import { getRandomName, getRandomCatPictureIndex } from '../../utils/catUtils';
import { BASE_URL } from './api.constant';
import { DEVICE_WIDTH } from '../../utils/deviceUtils';
import { TEXTS } from '../../shared/constants/index';


export class Api {
  // Helpers
  static get = async <T>(
    endpoint: string,
    params?: { [key in string]: string | number },
  ) => {
    const url = BASE_URL + endpoint;
    return (await axios.get<{ data: T }>(url, { params })).data
      .data;
  };

  static post = async <D, T>(endpoint: string, data: D) => {
    const url = BASE_URL + endpoint;
    return (await axios.post<{ data: T }>(url, data)).data.data;
  };

  static patch = async <D, T>(endpoint: string, data: D) => {
    const url = BASE_URL + endpoint;

    return (await axios.patch<{ data: T }>(url, data)).data.data;
  };

  static delete = async <D, T>(endpoint: string, data: D) => {
    const url = BASE_URL + endpoint;
    return (await axios.delete<{ data: T }>(url, { data })).data
      .data;
  };

  //#region Api methods  
  public static getCats = async (count: number = 0): Promise<Array<Cat>> => {
    const deviceWidth = DEVICE_WIDTH
    let id = 0;
    let result = [...Array<Cat>(count)].map(() => {
      return (
        {
          id: id++,
          name: getRandomName(),
          image: `${BASE_URL}${deviceWidth}/${deviceWidth}?image=${getRandomCatPictureIndex()}`,
          description: TEXTS.LOREM_IPSUM,
        }
      )
    }
    )
    return result
  }
  //#endregion
}
