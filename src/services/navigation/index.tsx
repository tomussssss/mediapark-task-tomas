import React from "react";
import Icon from "react-native-dynamic-vector-icons";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { isReadyRef, navigationRef } from "react-navigation-helpers";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { SCREENS } from "../../shared/constants/index";

//#region Screens

import { HomeScreen } from "../../screens/home";
import { DetailScreen } from "../../screens/detail";
//#endregion

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const Navigation = () => {
  React.useEffect((): any => {
    return () => (isReadyRef.current = false);
  }, []);

  const renderTabNavigation = () => {
    return (
      <Tab.Navigator
        screenOptions={() => ({
          tabBarIcon: ({ color, size }) => {
            let iconName: string = "logo-octocat";
            // You can return any component that you like here!
            return (
              <Icon name={iconName} type="Ionicons" size={size} color={color} />
            );
          },
        })}

        tabBarOptions={{
          activeTintColor: "#FFFFFF",
          inactiveTintColor: "gray",
          style: {
            backgroundColor: '#757575',
          },
        }}
      >
        <Tab.Screen name={SCREENS.HOME} component={HomeScreen} />
      </Tab.Navigator>
    );
  };

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        isReadyRef.current = true;
      }}
    >
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name={SCREENS.HOME} component={renderTabNavigation} />
        <Stack.Screen name={SCREENS.DETAIL}>
          {(props) => <DetailScreen {...props} />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
