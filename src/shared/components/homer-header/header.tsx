import TextWrapper from "../text-wrapper/TextWrapper"
import { catActions } from '../../state/cat/CatActions';
import React from 'react';
import { useState } from 'react';
import { Alert, Keyboard, Pressable, StyleSheet, Text, View } from 'react-native';
import NumericInput from 'react-native-numeric-input'
import { useDispatch } from 'react-redux';
import { toInteger } from "lodash";


const Header = (props) => {
    const { headerHeight } = props;
    const [value, setValue] = useState(20)
    const action = useDispatch();
    return (
        <>
            <View
                style={[
                    styles.subHeader,
                    {
                        height: headerHeight,
                        justifyContent: 'center'
                    },
                ]}>
                <TextWrapper color={'white'} style={styles.conversation}>Kittens</TextWrapper>
            </View>
            <View
                style={[
                    styles.subHeader,
                    {
                        height: headerHeight / 2,
                    },
                ]}>
                <View style={styles.searchBox}>
                    <TextWrapper color={'white'}>Number of kittens: </TextWrapper>
                    <NumericInput
                        value={value}
                        maxValue={100000}
                        minValue={0}
                        onChange={(value) => {
                            setValue(toInteger(value))
                        }}
                        totalWidth={100}
                        totalHeight={headerHeight / 4}
                        iconSize={10}
                        step={1}
                        valueType='real'
                        rounded
                        textColor='#FFFFFF'
                        iconStyle={{ color: '#757575' }}
                        rightButtonBackgroundColor='#FFFFFF'
                        leftButtonBackgroundColor='#FFFFFF' />
                    <Pressable onPress={() => {
                        Keyboard.dismiss()
                        if (value > 0 && value <= 100000) {
                            action(catActions.getCats(value));
                        } else {
                            Alert.alert('Wrong value', 'The renge of kittens to get should bet between 1 and 100000', [{ text: "OK" }]);
                            setValue(0)
                        }
                    }}>
                        <View style={[styles.button, { height: headerHeight / 4 }]}>
                            <TextWrapper color={'#757575'} style={{ fontSize: 12 }}>Refresh</TextWrapper>
                        </View>
                    </Pressable>

                </View>
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    subHeader: {
        width: '100%',
        paddingHorizontal: 10,
        backgroundColor: '#757575',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    conversation: { color: 'white', fontSize: 36, paddingTop: 30, fontWeight: 'bold' },
    searchText: {
        color: '#8B8B8B',
        fontSize: 17,
        lineHeight: 22,
        marginLeft: 8,
    },
    button: {
        paddingVertical: 8,
        paddingHorizontal: 10,
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    searchBox: {
        paddingVertical: 8,
        paddingHorizontal: 10,
        backgroundColor: '#757575',
        borderRadius: 10,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
});
export default Header;
