import React from 'react'
import { View, StyleSheet } from 'react-native'
import Modal from 'react-native-modal'
import FastImage from 'react-native-fast-image'
import TextWrapper from '../../components/text-wrapper/TextWrapper'
import { DEVICE_HEIGTH } from '../../../utils/deviceUtils'
import { StatusBar } from 'react-native'
import NetInfo from '@react-native-community/netinfo'
import { useEffect } from 'react'
import { useState } from 'react'


const TopBarPopup = () => {
    const [popupOpen, setPopupOpen] = useState(false)
    useEffect(() => {
        const unsubscribe = NetInfo.addEventListener(state => {
            if (!state.isConnected) {
                setPopupOpen(true)
            }
        });
        return () => {
            unsubscribe()
        }
    }, [])

    return (
        <Modal
            isVisible={popupOpen}
            onBackButtonPress={() => setPopupOpen(false)}
            style={styles.modalStyle}
            onSwipeComplete={() => setPopupOpen(false)}
            swipeDirection={['up']}
            swipeThreshold={50}
            animationIn={'slideInDown'}
            animationOut={'slideOutUp'}
            onBackdropPress={() => setPopupOpen(false)}
            backdropTransitionOutTiming={0}
            statusBarTranslucent={true}
            hasBackdrop={false}
            coverScreen={false}
            deviceHeight={DEVICE_HEIGTH}>
            {content()}
        </Modal>
    )
}

const content = () => {
    let image = (<FastImage source={require('../../../assets/no-connection.png')} style={{ width: 50, height: 50 }} />)
    return (
        <View style={styles.popupWrapper}>
            <View
                style={styles.buttonContainer}>
                <View style={{ padding: 10 }}>
                    {image}
                </View>

                <View style={styles.contentContainer}>
                    <TextWrapper adjustsFontSizeToFit={true}
                        color={'white'}
                        numberOfLines={2}
                        style={styles.popupText}>{'No internet connection!'}
                    </TextWrapper>
                </View>

            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    modalStyle: {
        marginLeft: 0,
        marginTop: StatusBar.currentHeight,
        justifyContent: 'flex-start',
        width: '100%',
        height: 0
    },
    popupWrapper: {
        backgroundColor: '#757575',
        borderWidth: 1,
        padding: 20,
        margin: 20,
        borderRadius: 20

    },
    buttonContainer: {
        flexDirection: 'row',
        textAlign: 'center'
    },
    contentContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        shadowOpacity: 2
    },
    popupText: {
        textAlignVertical: 'center',
        textAlign: 'center',

    }
})

export default TopBarPopup
