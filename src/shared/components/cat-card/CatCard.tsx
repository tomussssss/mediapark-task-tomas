import TextWrapper from "../text-wrapper/TextWrapper"
import React from 'react';
import { Pressable, StyleSheet, View } from 'react-native';
import FastImage from 'react-native-fast-image'
import * as NavigationService from "react-navigation-helpers";
import { DEVICE_WIDTH } from "../../../utils/deviceUtils";


interface CatProps {
    cat: Cat,
    size: number,
}

export const CatCard: React.FC<CatProps> = ({ cat }) => {

    return (
        <Pressable onPress={() => {
            NavigationService.navigate('Detail', { cat: cat })
        }}>
            <View style={styles.container}>
                <View style={styles.innerContainer}>
                    <View>
                        <FastImage
                            source={{
                                uri: cat?.image,
                            }}
                            style={{ width: Math.floor(DEVICE_WIDTH * 0.9), height: Math.floor(DEVICE_WIDTH * 0.9) }}
                        />
                    </View>
                    <View style={styles.textContainer}>
                        <TextWrapper color={'white'}>{cat.name}</TextWrapper>
                    </View>
                </View>
            </View>
        </Pressable>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        paddingVertical: 30,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: 'gray',
        shadowOpacity: 1,
        elevation: 4,
    },
    innerContainer: {
        borderRadius: 10,
        backfaceVisibility: 'hidden',
        overflow: "hidden",
    },
    textContainer: {
        backgroundColor: '#757575',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
    },
});
