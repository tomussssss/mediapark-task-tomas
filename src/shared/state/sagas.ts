import { call, takeEvery } from 'redux-saga/effects';

import { constants } from './constants';
import { getCats } from './cat/CatSaga';

export function* rootSaga() {
  yield takeEvery(constants.cat.GET_CATS, getCats);
  yield call(console.log, 'Hello from saga');
}
