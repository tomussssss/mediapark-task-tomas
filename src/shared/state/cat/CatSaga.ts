import { Api } from '../../../services/api';
import { put } from 'redux-saga/effects';

import { actions } from '../actions';

type Params = { count: number, type: number }


function* getCats({ count }: Params) {
  try {
    const response = yield (Api.getCats(count));
    yield put(actions.cat.catActions.setCats(response));
  } catch (e) {
    console.log(e);
  }
}

export { getCats };
