import { constants } from '../constants';

const getCats = (count: number) => ({
  type: constants.cat.GET_CATS,
  count: count
});

const setCats = (cats: Cat[]) => ({
  type: constants.cat.SET_CATS,
  cats,
});

export const catActions = {
  getCats,
  setCats,
};
