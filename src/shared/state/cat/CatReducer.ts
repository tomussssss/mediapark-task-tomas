import { createReducer } from '@reduxjs/toolkit';

import { constants } from '../constants';

export const INITIAL_STATE: CatReducerState = {
  cats: [],
  loading: true,
};

export interface CatReducerState {
  cats: Cat[];
  loading: boolean;
}

export const catReducer = createReducer(INITIAL_STATE, {
  [constants.cat.SET_CATS]: (state, action) => {
    return {
      cats: action.cats,
      loading: false,
    };
  },
  [constants.cat.GET_CATS]: (state, action) => {
    return {
      loading: true,
    };
  },
});
