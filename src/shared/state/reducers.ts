import { AnyAction, CombinedState, combineReducers } from 'redux';
import { catReducer, CatReducerState } from './cat/CatReducer';

export interface RootState {
  cat: CatReducerState;
}

export interface PersistedAppState extends RootState {
  _persist: { version: number; rehydrated: boolean };
}

const combinedReducer = combineReducers<CombinedState<RootState>>({
  cat: catReducer,
});

const rootReducer = (state: RootState | undefined, action: AnyAction) => {
  return combinedReducer(state, action);
};

export { rootReducer };
