import React from "react";
import { Image, View } from "react-native";
import FastImage from 'react-native-fast-image'
import { StyleSheet } from "react-native";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import { ImageHeaderScrollView } from 'react-native-image-header-scroll-view';
import { DEVICE_WIDTH } from "../../utils/deviceUtils";

interface MainProps {

}

export const DetailScreen: React.FC<MainProps> = (props) => {
  return (
    <ImageHeaderScrollView
      maxHeight={DEVICE_WIDTH}
      minHeight={50}
      renderHeader={() => (<Image source={{
        uri: props?.route?.params?.cat?.image,
      }} style={{ width: DEVICE_WIDTH, height: DEVICE_WIDTH }} />)}
      renderForeground={() => (
        <View style={styles.innerContainer} >
          <View
            style={styles.catName}>
            <TextWrapper color={'white'} style={{ padding: 20 }}>{props?.route?.params?.cat?.name}</TextWrapper>
          </View>
        </View>
      )}
    >
      <View style={styles.container}>
        <TextWrapper color={'white'}>{props?.route?.params?.cat?.description}</TextWrapper>
      </View>
    </ImageHeaderScrollView >
  );
}


const styles = StyleSheet.create({
  container: {
    padding: 30,
    height: DEVICE_WIDTH * 2,
    backgroundColor: '#757575'
  },
  innerContainer: {
    height: DEVICE_WIDTH * 1.8,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
  },
  catName: {
    borderRadius: 50,
    backgroundColor: '#757575'
  },
});

