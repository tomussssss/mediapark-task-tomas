import React from "react";
import { RootState } from '../../shared/state/reducers';
import { connect, MapStateToProps, useDispatch } from 'react-redux';
import { ActivityIndicator, Keyboard, Platform, Pressable, StatusBar, StyleSheet } from "react-native";
import { CatCard } from "../../shared/components/cat-card/CatCard";
import { DEVICE_WIDTH } from "../../utils/deviceUtils";
import { useRef } from "react";
import { Animated } from "react-native";
import Header from "../../shared/components/homer-header/header";
import { useEffect } from "react";
import { catActions } from "../../shared/state/cat/CatActions";

const { diffClamp } = Animated;
const headerHeight = 116;

interface MainProps {
  loading: boolean;
  cats: Cat[]
}

interface DispatchProps {
  getCats: () => Cat[];
}



const HomeScreen: React.FC<MainProps & DispatchProps> = ({
  loading,
  cats
}) => {
  const action = useDispatch();
  useEffect(() => {
    action(catActions.getCats(20));
  }, []);

  const ref = useRef(null);

  const scrollY = useRef(new Animated.Value(0));
  const scrollYClamped = diffClamp(scrollY.current, 0, headerHeight);

  const translateY = scrollYClamped.interpolate({
    inputRange: [0, headerHeight],
    outputRange: [0, -(headerHeight)],
  });

  const translateYNumber = useRef();

  translateY.addListener(({ value }) => {
    translateYNumber.current = value;
  });

  const renderFlastlistFooter = () => {
    if (!loading) return null;
    return (
      <ActivityIndicator
        size="large" color="#0000ff"
      />
    );
  };


  const handleScroll = Animated.event(
    [
      {
        nativeEvent: {
          contentOffset: { y: scrollY.current },
        },
      },
    ],
    {
      useNativeDriver: true,
    },
  );

  if (loading && cats?.length === 0) {
    return (
      <ActivityIndicator size="large" color="#0000ff" />
    )
  } else {
    return (
      <>
        <Pressable onPress={() => {
          Keyboard.dismiss()
        }}>
          <StatusBar backgroundColor="#757575" style="light" />
          <Animated.View style={[styles.header, { transform: [{ translateY }] }]}>
            <Header {...{ headerHeight }} />
          </Animated.View>
          <Animated.FlatList
            scrollEventThrottle={16}
            contentContainerStyle={{ paddingTop: Platform.OS === 'android' ? headerHeight + 80 : headerHeight + 50 }}
            onScroll={handleScroll}
            ListFooterComponent={renderFlastlistFooter}
            ref={ref}
            data={cats}
            extraData={cats}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }: { item: Cat }) => <CatCard cat={item} size={DEVICE_WIDTH}></CatCard>}
          />
        </Pressable>

      </>
    );
  }
}



const mapStateToProps: MapStateToProps<
  MainProps,
  MainProps & DispatchProps,
  RootState
> = state => ({
  loading: state.cat.loading,
  cats: state.cat.cats
});

const screen = connect(mapStateToProps)(HomeScreen);
export { screen as HomeScreen };

const styles = StyleSheet.create({
  header: {
    position: 'absolute',
    backgroundColor: '#757575',
    left: 0,
    right: 0,
    paddingTop: 40,
    width: '100%',
    paddingBottom: 0,
    zIndex: 1,
  },
  subHeader: {
    height: headerHeight / 2,
    width: '100%',
    paddingHorizontal: 10,
  },
  container: {
    flex: 1,
  },
});